import { Group, Person, createPerson } from "./person";
import {
  ZombieVariant,
  infectWithZombieA,
  infectWithZombieB,
  infectWithZombie32,
  infectWithZombieC,
  infectWithZombieUltime,
} from "./zombie";
import { ConsoleFormatter } from "./consoleFormatter";
import { vaccinateA1, vaccinateB1, vaccinateUltimate } from "./vaccine";

const luke: Person = createPerson("Luke Skywalker", 30);
const leia: Person = createPerson("Leia Organa", 60);
const han: Person = createPerson("Han Solo", 25);
const chewbacca: Person = createPerson("Chewbacca", 100);
const obiWan: Person = createPerson("Obi-Wan Kenobi", 60);
const darthVader: Person = createPerson("Darth Vader", 45);
const yoda: Person = createPerson("Yoda", 900);
const r2d2: Person = createPerson("R2-D2", 50);
const c3po: Person = createPerson("C-3PO", 60);
const padme: Person = createPerson("Padmé Amidala", 30);
const jarjar: Person = createPerson("JarJar Binks", 25);
const anakin: Person = createPerson("Anakin Skywalker", 10);
const jabba: Person = createPerson("Jabba The Hutt", 110);

const group1: Group = { groupName: "Group1", members: [], subgroups: [] };
const group2: Group = { groupName: "Group2", members: [], subgroups: [] };
const group3: Group = { groupName: "Group3", members: [], subgroups: [] };
const group4: Group = { groupName: "Group4", members: [], subgroups: [] };
const group5: Group = { groupName: "Group5", members: [], subgroups: [] };

group1.members.push(luke, leia);
group2.members.push(han, chewbacca);
group3.members.push(obiWan, darthVader);
group4.members.push(yoda, r2d2, c3po, anakin);
group5.members.push(padme, jarjar, jabba);

group1.subgroups.push(group2);
group2.subgroups.push(group3);
group4.subgroups.push(group5);

const allGroups: Group[] = [group1, group2, group3, group4, group5];

console.log(" ---- PREMIERE PHASE D'INFECTION ---- ");
console.log(" ---- CHEWIE INFECTE PAR ZOMBIE A ---- !\n ");

infectWithZombieA(chewbacca as Person, allGroups);

console.log(ConsoleFormatter.formatAllGroups([group1]));
console.log(ConsoleFormatter.formatAllGroups([group4]));

console.log(" ---- PREMIERE PHASE DE VACCINATION ---- ");
console.log(" ---- VACCINATION VIA VACCINATE-A1 ---- \n ");

vaccinateA1(allGroups);

console.log(ConsoleFormatter.formatAllGroups([group1]));
console.log(ConsoleFormatter.formatAllGroups([group4]));

console.log(" ---- DEUXIEME PHASE D'INFECTION ---- ");
console.log(" ---- YODA INFECTE PAR ZOMBIE C ---- !\n");

infectWithZombieC(yoda as Person, allGroups);

console.log(ConsoleFormatter.formatAllGroups([group1]));
console.log(ConsoleFormatter.formatAllGroups([group4]));

console.log(" ---- DEUXIEME PHASE DE VACCINATION ----  ");
console.log(" ---- VACCINATION VIA VACCINATE-B1 ---- \n ");

vaccinateB1(allGroups);

console.log(ConsoleFormatter.formatAllGroups([group1]));
console.log(ConsoleFormatter.formatAllGroups([group4]));

console.log(" ---- TROISIEME PHASE D'INFECTION ----");
console.log(" ---- PADME INFECTE PAR ZOMBIE B ! ---- \n");

infectWithZombieB(padme as Person, allGroups);

console.log(ConsoleFormatter.formatAllGroups([group1]));
console.log(ConsoleFormatter.formatAllGroups([group4]));

console.log(" ---- TROISIEME PHASE DE VACCINATION ----  ");
console.log(" ---- VACCINATION VIA VACCINATE-B1 ---- \n ");

vaccinateB1(allGroups);

console.log(ConsoleFormatter.formatAllGroups([group1]));
console.log(ConsoleFormatter.formatAllGroups([group4]));

console.log(" ---- QUATRIEME PHASE D'INFECTION ----");
console.log(" ---- INFECTION VIA ZOMBIE ULTIME ! ---- \n");

infectWithZombieUltime(allGroups);

console.log(ConsoleFormatter.formatAllGroups([group1]));
console.log(ConsoleFormatter.formatAllGroups([group4]));

console.log(" ---- QUATRIEME PHASE DE VACCINATION ----  ");
console.log(" ---- VACCINATION VIA VACCINATE-ULTIME ---- \n ");

vaccinateUltimate(allGroups);

console.log(ConsoleFormatter.formatAllGroups([group1]));
console.log(ConsoleFormatter.formatAllGroups([group4]));
