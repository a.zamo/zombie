import { ZombieVariant } from "./zombie";

export interface Person {
  personName: string;
  age: number;
  isInfected: boolean;
  isImmune: boolean;
  isAlive: boolean;
  exposedToVariant: ZombieVariant | null;
}

export interface Group {
  groupName: string;
  members: (Person | Group)[];
  subgroups: Group[];
}

// Utils

export function createPerson(personName: string, age: number): Person {
  return {
    personName,
    age,
    isInfected: false,
    isImmune: false,
    isAlive: true,
    exposedToVariant: null,
  };
}

export function isPerson(obj: any): obj is Person {
  return (
    obj &&
    obj.isInfected !== undefined &&
    obj.isAlive !== undefined &&
    obj.isImmune !== undefined
  );
}

export function isGroup(obj: any): obj is Group {
  return (
    obj &&
    obj.groupName !== undefined &&
    obj.members !== undefined &&
    obj.subgroups !== undefined
  );
}

export function findParentGroup(
  childGroup: Group,
  allGroups: Group[]
): Group | null {
  for (const group of allGroups) {
    if (group.subgroups.includes(childGroup)) {
      return group;
    }
  }
  return null;
}

export function findSocialGroup(
  person: Person,
  allGroups: Group[]
): Group | undefined {
  for (const group of allGroups) {
    if (group.members.includes(person)) {
      return group;
    }
  }
  return undefined;
}

export function findRootPerson(groups: Group[]): Person | null {
  let rootPerson: Person | null = null;
  for (const group of groups) {
    if (group.members.length > 0) {
      for (const member of group.members) {
        if ("personName" in member && !member.isInfected) {
          if (!rootPerson) {
            rootPerson = member as Person;
          }
        }
      }
    }
  }
  return rootPerson;
}
