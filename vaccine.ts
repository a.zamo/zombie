import { Person, Group, isPerson, isGroup } from "./person";
import { ZombieVariant } from "./zombie";

export function vaccinateA1(allGroups: Group[]): void {
  for (const group of allGroups) {
    for (const member of group.members) {
      if (isPerson(member)) {
        const person = member as Person;
        if (person.isAlive && person.age >= 0 && person.age <= 30) {
          if (
            person.isInfected &&
            (person.exposedToVariant === ZombieVariant.ZombieA ||
              person.exposedToVariant === ZombieVariant.Zombie32)
          ) {
            person.isInfected = false;
            person.exposedToVariant = null;
            person.isImmune = true;
          }
        }
      } else if (isGroup(member)) {
        const subgroup = member as Group;
        vaccinateA1(subgroup.subgroups);
      }
    }
  }
}

export function vaccinateB1(allGroups: Group[]): void {
  let cureCount = 0;

  for (const group of allGroups) {
    for (const member of group.members) {
      if (isPerson(member)) {
        const person = member as Person;
        if (
          person.isAlive &&
          person.isInfected &&
          (person.exposedToVariant === ZombieVariant.ZombieB ||
            person.exposedToVariant === ZombieVariant.ZombieC)
        ) {
          if (cureCount % 2 === 0) {
            person.isInfected = false;
            person.exposedToVariant = null;
          } else {
            person.isAlive = false;
          }
          cureCount++;
        }
      }
    }
  }
}

export function vaccinateUltimate(allGroups: Group[]): void {
  for (const group of allGroups) {
    for (const member of group.members) {
      if (
        isPerson(member) &&
        member.isInfected &&
        member.exposedToVariant === ZombieVariant.ZombieUltime
      ) {
        member.isInfected = false;
        member.isImmune = true;
        member.exposedToVariant = null;
      }
    }
  }
}
