import { Person, Group } from "./person";
import { ZombieVariant } from "./zombie";

export class ConsoleFormatter {
  static colorizeText(text: string, colorCode: string): string {
    return `\x1b[${colorCode}m${text}\x1b[0m`;
  }

  static formatGroup(
    group: Group,
    indent: string = "",
    level: number = 0
  ): string {
    let formattedGroup = `${indent}\x1b[1mGroupe: ${group.groupName}\x1b[0m\n`;
    for (const member of group.members) {
      if ("personName" in member) {
        // Si c'est une personne
        formattedGroup += this.formatPerson(member as Person, `${indent}  `);
      } else {
        // Si c'est un sous-groupe
        formattedGroup += this.formatGroup(
          member as Group,
          `${indent}${level % 2 === 0 ? "|" : " "}  `,
          level + 1
        );
      }
    }
    return formattedGroup;
  }

  static formatPerson(person: Person, indent: string = ""): string {
    let personInfo = `${indent}${person.personName} - ${person.age} ans - `;
    if (person.isAlive) {
      personInfo += "Vivant";
      if (!person.isImmune && !person.isInfected) {
        personInfo += " et en bonne santé";
      }
      if (person.isImmune) {
        personInfo += " et Immunisé";
      }
      if (person.isInfected) {
        personInfo += ` et Infecté par le Variant ${person.exposedToVariant}`;
      }
    } else {
      personInfo += "Mort";
    }

    if (person.isAlive && person.isImmune) {
      personInfo = this.colorizeText(personInfo, "34"); // Bleu pour immunisé
    } else if (person.isAlive && person.isInfected) {
      personInfo = this.colorizeText(personInfo, "33"); // Jaune pour infecté
    } else if (!person.isAlive) {
      personInfo = this.colorizeText(personInfo, "31"); // Rouge pour mort
    } else {
      personInfo = this.colorizeText(personInfo, "32"); // Vert pour simplement vivant
    }

    return personInfo + "\n";
  }

  static formatAllGroups(
    groups: Group[],
    indent: string = "",
    level: number = 0
  ): string {
    let formattedGroups = "";
    for (const group of groups) {
      formattedGroups += this.formatGroup(group, indent, level);
      formattedGroups += this.formatAllGroups(
        group.subgroups,
        `${indent}${level % 2 === 0 ? "|" : " "}  `,
        level + 1
      ); // Utilisation de caractères spéciaux pour l'indentation
    }
    return formattedGroups;
  }
}
