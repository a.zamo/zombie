import {
  Group,
  Person,
  findParentGroup,
  findRootPerson,
  findSocialGroup,
  isPerson,
} from "./person";

export enum ZombieVariant {
  ZombieA = "Zombie-A",
  ZombieB = "Zombie-B",
  Zombie32 = "Zombie-32",
  ZombieC = "Zombie-C",
  ZombieUltime = "Zombie-Ultime",
}
export function infectWithZombieA(person: Person, groups: Group[]): void {
  if (person.isInfected || !person.isAlive || person.isImmune) return;
  person.isInfected = true;
  person.exposedToVariant = ZombieVariant.ZombieA;

  for (const group of groups) {
    if (group.members.includes(person)) {
      for (const member of group.members) {
        if (member !== person && "personName" in member && !member.isInfected) {
          member.isInfected = true;
          member.exposedToVariant = ZombieVariant.ZombieA;
        }
      }

      for (const subgroup of group.subgroups) {
        for (const member of subgroup.members) {
          if ("personName" in member && !member.isInfected) {
            infectWithZombieA(member as Person, subgroup.subgroups); // Récursivement infecter les personnes dans les sous-groupes descendants
          }
        }
      }

      break;
    }
  }
}

export function infectWithZombieB(person: Person, allGroups: Group[]): void {
  if (person.isInfected || !person.isAlive || person.isImmune) return;

  person.isInfected = true;
  person.exposedToVariant = ZombieVariant.ZombieB;

  // Infectez les personnes dans les groupes ascendants
  for (const group of allGroups) {
    if (group.members.includes(person)) {
      // Infectez les personnes dans les groupes ascendants
      let currentGroup: Group | null = group;
      while (currentGroup) {
        for (const member of currentGroup.members) {
          if ("personName" in member && !member.isInfected) {
            member.isInfected = true;
            member.exposedToVariant = ZombieVariant.ZombieB;
          }
        }
        currentGroup = findParentGroup(currentGroup, allGroups);
      }
      break;
    }
  }
}

export function infectWithZombie32(person: Person, allGroups: Group[]): void {
  const infectPersonAndRelatives = (
    p: Person,
    currentGroups: Group[],
    ascending: boolean = true
  ) => {
    if (p.isInfected || !p.isAlive || p.isImmune || p.age < 32) {
      return;
    }

    p.isInfected = true;
    p.exposedToVariant = ZombieVariant.Zombie32;

    for (const group of allGroups) {
      if (
        currentGroups.length > 0 &&
        group === currentGroups[currentGroups.length - 1]
      ) {
        continue; // Éviter de réinfecter le groupe actuel
      }

      for (const relativeMember of group.members) {
        if ("personName" in relativeMember) {
          infectPersonAndRelatives(
            relativeMember as Person,
            [...currentGroups, group],
            ascending
          );
        }
      }
    }

    if (!ascending) {
      for (const group of allGroups) {
        if (
          currentGroups.length > 0 &&
          group === currentGroups[currentGroups.length - 1]
        ) {
          continue; // Éviter de réinfecter le groupe actuel
        }

        for (const relativeGroup of group.subgroups) {
          for (const relativeMember of relativeGroup.members) {
            if ("personName" in relativeMember) {
              infectPersonAndRelatives(
                relativeMember as Person,
                [...currentGroups, relativeGroup, group],
                ascending
              );
            }
          }
        }
      }
    }
  };

  infectPersonAndRelatives(person, [], true);
  infectPersonAndRelatives(person, [], false);
}

export function infectWithZombieC(person: Person, allGroups: Group[]): void {
  if (!person.isAlive || person.isImmune) return;

  // Infectez cette personne avec le Zombie-C
  person.isInfected = true;
  person.exposedToVariant = ZombieVariant.ZombieC;

  const socialGroup = findSocialGroup(person, allGroups);

  if (socialGroup) {
    let infectNext = false;
    for (const member of socialGroup.members) {
      if (member !== person && "personName" in member && !member.isInfected) {
        if (infectNext) {
          member.isInfected = true;
          member.exposedToVariant = ZombieVariant.ZombieC;
        }
        infectNext = !infectNext;
      }
    }
  }
}

export function infectWithZombieUltime(allGroups: Group[]): void {
  // Trouvez la personne racine la plus ascendante
  const rootPerson = findRootPerson(allGroups);

  if (rootPerson) {
    // Infectez la personne racine avec le Zombie-Ultime
    rootPerson.isInfected = true;
    rootPerson.exposedToVariant = ZombieVariant.ZombieUltime;
  }
}
