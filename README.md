## Prérequis

Avant de lancer le projet, assurez-vous d'avoir **TypeScript** installé dans votre environnement. Vous pouvez vérifier cela en exécutant la commande suivante dans votre terminal :

```bash
tsc --version
```

Si TypeScript n'est pas installé, vous pouvez l'installer globalement via npm avec :

```bash
npm install -g typescript
```

## Lancement du projet

Pour exécuter le projet, suivez ces étapes :

1. **Compilation du code TypeScript :**
   Ouvrez votre terminal et naviguez jusqu'au répertoire du projet. Compilez le code TypeScript en JavaScript en exécutant :

   ```bash
   tsc
   ```

   Cette commande va compiler tous les fichiers `.ts` dans votre projet en fichiers `.js`.

2. **Exécution du projet :**
   Après la compilation, vous pouvez lancer le fichier principal JavaScript avec Node.js :

   ```bash
   node main.js
   ```

   Remplacez `main.js` par le chemin d'accès de votre fichier JavaScript principal si son nom est différent.

N'hésitez pas à ouvrir une issue ou à me contacter directement si vous rencontrez des problèmes lors de l'exécution de ces commandes.
